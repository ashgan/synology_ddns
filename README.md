# add nsupdate.info as ddns service provider
_**since dsm 6.0, require more work: this don't work anymore.
filesystem change a bit, and I see abuse reports from nsupdate.info**_

This require ssh access to the synology and editing 2 files on the system.  
_**All modifications are deleted by synology updates**_

Enable ssh access with the synology configuration pannel, then ssh synology with root user.  
root password is the same as the admin user the NAS setup for you at first use.

upload via scp or create nsupdate_info.php in /usr/syno/bin/ddns/  
don't forget to make a  
`chmod +x /usr/syno/bin/ddns/nsupdate_info.php`

edit /etc.defaults/php/conf.d/user-settings.ini and append `:/usr/syno/bin/ddns/` to the open_basedir variable.

it should look like this:

`open_basedir = /etc.defaults:/etc:/usr/syno/synoman:/tmp:/var/services/tmp:/var/services/web:/var/services/homes:/usr/syno/bin/ddns/`

edit /etc.defaults/ddns_providers.conf to add this 3 lines at the end of the file.  
Don't mess up with other sections, or you will break other ddns settings.
(vim or nano not available on Synology, only vi)

	[nsupdate.info]
	    modulepath=/usr/syno/bin/ddns/nsupdate_info.php
	    queryurl=https://__HOSTNAME__:__PASSWORD__@ipv4.nsupdate.info/nic/update

changes are immediate after saving your work.  

On Synology ddns pannel, you will find the new nsupdate.info provider.  

Add a new host and edit fields like this:  
hostname: your nsupdate.info hostname  
username: your nsupdate.info hostname (not required by the script, but mandatory for the form)  
password: your nsupdate.info secret key  

tested and working on a DS414j since DSM 5.1-5021 update 2
